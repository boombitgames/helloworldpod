//
//  CORTestClass.mm
//  HelloWorldPod
//
//  Created by Michał Gliniecki on 12/08/2020.
//

#if defined(__IPHONE_14_0)
#import <AppTrackingTransparency/ATTrackingManager.h>
#endif
#import <AdSupport/AdSupport.h>

#import <CORCommon/CORUnityInterface.h>

extern "C" {
void HelloWorld() {
    NSLog(@"-----> Hello World FROM iOS!");
    CORUnitySendMessage("core_idfa_consent_view", "OnMessageFromXcodeReceived", "msg passed by USM");
}

typedef void (*GetValueFromXcodeDelegate)(const char* msg);

void RegisterCallback(GetValueFromXcodeDelegate func) {
    NSLog(@"-----> Method with delegate");
    func("Hello from the Xcode side!");
}

int CORCanAskForIdfa() {
#if defined(__IPHONE_14_0)
    if(@available(iOS 14.0, macOS 11.0, tvOS 14.0, *)) {
        ATTrackingManagerAuthorizationStatus status = [ATTrackingManager trackingAuthorizationStatus];
        
        int idfaStatus = (int)status;
        
        NSLog(@"IDFA popup status: %d", idfaStatus);
        
        return idfaStatus;
    }
#endif
    return -1;
}
}
